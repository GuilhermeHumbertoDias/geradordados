package application;

import br.com.caelum.stella.DigitoPara;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import javafx.stage.Window;
import br.com.caelum.stella.format.CPFFormatter;
import format.MatriculaFormatter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Scanner;

public class GeradorController {
	
	@FXML
	public Button btnGerar;	
	@FXML
	public Spinner<Integer> spnQuantidade;
	@FXML
	public CheckBox chkCpf;	
	@FXML	
	public CheckBox chkMatricula;	
	@FXML
	public CheckBox chkNome;	
	@FXML
	public TextArea txtDados;	
	@FXML
	public Button btnExportar;	
	@FXML
	public TextField txfSep;	
	@FXML
	public FileChooser fileChooser;	
	
	@FXML
	public void initialize() {
		int initialValue = 1;	
		chkCpf.setSelected(true);
		chkNome.setSelected(true);
		chkMatricula.setSelected(true);
				
		txfSep.setText(":");
		SpinnerValueFactory<Integer> valueFactory = 
                new SpinnerValueFactory.IntegerSpinnerValueFactory(1, 1000, initialValue);		
		spnQuantidade.setValueFactory(valueFactory);
		lerNomes();
		
		Image gerarIcone = new Image(getClass().getResourceAsStream("list.png"));
		Image salvarIcone = new Image (getClass().getResourceAsStream("folder.png"));
		
		btnGerar.setGraphic(new ImageView(gerarIcone));
		btnExportar.setGraphic(new ImageView(salvarIcone));
	}
	
	// ArrayList Name
	ArrayList<String> nome = new ArrayList<String>();				
	// ArrayList LastName
	ArrayList <String> sobrenome = new ArrayList<String>();
	
	
	
	@FXML
	public void geradorDados() {
		
		
		
		Integer contador = spnQuantidade.getValue();		
		txtDados.setText("");
		
		for (int i = 0; i < contador; i++) {
			Boolean sep = false;
			String tSep = txfSep.getText();
					
			if (chkCpf.isSelected()) {
				txtDados.appendText("CPF: " + generateCPF());
				sep = true;
			} 		
			if (chkMatricula.isSelected()) {
				if (sep) {
					txtDados.appendText(" "+tSep+" ");
				} 	
				txtDados.appendText("Matrícula: " + generateMatricula());
				sep = true;
			} 			
			if (chkNome.isSelected()) {
				if (sep) {
					txtDados.appendText(" "+tSep+" ");
				} 	
				sep = true;
				txtDados.appendText("Nome: " + nome.get((int)(Math.random()*nome.size()))
									   + " " + sobrenome.get((int)(Math.random()*sobrenome.size())));
			}
			txtDados.appendText("\r\n");
        } 
	}
			
			
	private void lerNomes()  {
		
		  Scanner scanner;
		
		  InputStream is = getClass().getResourceAsStream("/nomes.txt");
	      scanner2 = new Scanner(is);
		  scanner = scanner2.useDelimiter("\\||\\n");
	                
			while (scanner.hasNext()) {
				
				String nomes = scanner.next();				
				String sobrenomes = scanner.next();
				
				nome.add(nomes);
				sobrenome.add(sobrenomes);
			}
			
		} 

	@FXML
	public void exportarArquivo() throws IOException  {
		
		String dadosGerados = txtDados.getText();
		dadosGerados = dadosGerados.replaceAll("\n", "\r\n");
		FileWriter arquivo;
		
		try {
			arquivo = new FileWriter(new File("Arquivo.txt"));
			arquivo.write(dadosGerados);
			arquivo.close();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}	
		
		 FileChooser fileChooser = new FileChooser();
		 FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("TXT files (*.txt)", "*.txt");
		 fileChooser.getExtensionFilters().add(extFilter);
		 
		 Window primaryStage = null;
		 
		 File file = fileChooser.showSaveDialog(primaryStage);
		 
		 if (file != null) {
			 SaveFile(txtDados.getText(), file);
		 }
		
	}

	private void SaveFile(String content, File file) throws IOException {
		
		String dadosNovos = txtDados.getText();		
		dadosNovos = dadosNovos.replaceAll("\n", "\r\n");
		
		try {
			FileWriter fileWriter;			
			fileWriter = new FileWriter(file);
			 fileWriter.write(dadosNovos);
			 fileWriter.close();			 
		} catch (IOException ex) {
			
		}
	}

	public static final CPFFormatter CPF_FORMATTER = new CPFFormatter();
    public static final MatriculaFormatter MATRICULA_FORMATTER = new MatriculaFormatter();
	private Scanner scanner2;
	
    private static String generateCPF() {
        String rand = String.format("%09d", (int)(Math.random() * 1000000000));
        DigitoPara dp = new DigitoPara(rand);
    	dp.comMultiplicadoresDeAte(2, 11).complementarAoModulo().trocandoPorSeEncontrar("0",10,11).mod(11);
    	String digito1 = dp.calcula();
    	String digito2 = dp.addDigito(digito1).calcula();
    	return CPF_FORMATTER.format(rand + digito1 + digito2);
    }
	
	private static String generateMatricula() {
        String rand = String.format("%06d", (int)(Math.random() * 1000000));
        DigitoPara dp = new DigitoPara(rand);
        dp.complementarAoModulo().trocandoPorSeEncontrar("0", 10, 11);
    	String digito1 = dp.calcula();
    	return MATRICULA_FORMATTER.format(rand + digito1);
    }
	
}