package format;

import java.text.NumberFormat;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

import br.com.caelum.stella.format.CPFFormatter;
import format.MatriculaFormatter;
import javafx.util.converter.NumberStringConverter;

public class Formats {
	public static final CPFFormatter CPF = new CPFFormatter();
    public static final MatriculaFormatter MATRICULA = new MatriculaFormatter();
    
    public static NumberFormat getDecimalFormat(int minFrac, int maxFrac, int minInt, int maxInt) {
        return new NumberFormatBuilder()
                .minFraction(minFrac)
                .maxFraction(maxFrac)
                .minInteger(minInt)
                .maxInteger(maxInt).build();
    }

    public static class NumberFormatBuilder {
        NumberFormat format;

        public NumberFormatBuilder() {
            format = NumberFormat.getNumberInstance();
        }

        public NumberFormatBuilder(Locale locale) {
            format = NumberFormat.getNumberInstance(locale);
        }

        public NumberFormatBuilder minInteger(int min) {
            format.setMinimumIntegerDigits(min);
            return this;
        }

        public NumberFormatBuilder maxInteger(int max) {
            format.setMaximumIntegerDigits(max);
            return this;
        }

        public NumberFormatBuilder minFraction(int min) {
            format.setMinimumFractionDigits(min);
            return this;
        }

        public NumberFormatBuilder maxFraction(int max) {
            format.setMaximumFractionDigits(max);
            return this;
        }

        public NumberFormat build() {
            return format;
        }
    }
}
