package format;

import java.util.regex.Pattern;

import br.com.caelum.stella.format.BaseFormatter;
import br.com.caelum.stella.format.Formatter;

public class MatriculaFormatter implements Formatter {

	 private static final String ZEROS = "00000000";
	    public static final Pattern FORMATED = Pattern.compile("(\\d{3,4}).?(\\d{3})-(\\d{1})");
	    public static final Pattern UNFORMATED = Pattern.compile("(\\d{3,4})(\\d{3})(\\d{1})");

	    private final BaseFormatter base;

	    public MatriculaFormatter() {
	        this.base = new BaseFormatter(FORMATED, "$1$2-$3", UNFORMATED, "$1$2$3");
	    }

	@Override
	public String format(String value) throws IllegalArgumentException {
		value = (ZEROS + value).substring(value.length());
        return base.format(value);
	}
	
	@Override
	public String unformat(String value) throws IllegalArgumentException {
		return base.unformat(value);
	}
	
	@Override
	public boolean isFormatted(String value) {
		return base.isFormatted(value);
	}
	
	@Override
	public boolean canBeFormatted(String value) {
		return base.canBeFormatted(value);
	}

	
}
